package test;

import org.springframework.data.repository.CrudRepository;

import demo.City;

public interface CityRepository extends CrudRepository<City, Integer>{
	

}
